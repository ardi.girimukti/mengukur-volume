package com.arduhadma.hitungvolume

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var edtWidth: EditText
    private lateinit var edtHight: EditText
    private lateinit var edtLength: EditText
    private lateinit var btnCalculate: Button
    private lateinit var tvResult: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        edtWidth = findViewById(R.id.edt_width)
        edtHight = findViewById(R.id.edt_hight)
        edtLength = findViewById(R.id.edt_length)
        btnCalculate = findViewById(R.id.btn_calculate)
        tvResult = findViewById(R.id.tv_result)

        btnCalculate.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_calculate){
            val inputLength = edtLength.text.toString().trim()
            val inputWidth = edtWidth.text.toString().trim()
            val inputHight = edtHight.text.toString().trim()

            var isEmptyFields = false
            var isInvalidDouble = false

            if (inputLength.isEmpty()){
                isEmptyFields = true
                edtLength.error = "Nilai ini tidak boleh kososng"
            }

            if (inputHight.isEmpty()){
                isEmptyFields = true
                edtHight.error = "Nilai ini tidak boleh kosong"
            }

            if (inputWidth.isEmpty()){
                isEmptyFields = true
                edtWidth.error = "Nilai ini tidak boleh kosong"
            }

            val length = toDouble (inputLength)
            val width = toDouble(inputWidth)
            val hight = toDouble(inputHight)

            if (width == null){
                isInvalidDouble = true
                edtWidth.error = "Hanya masukkan angka BODOH"
            }
            if (length == null){
                isInvalidDouble = true
                edtWidth.error = "Hanaya masukan angka BODOH"
            }
            if (hight == null){
                isInvalidDouble = true
                edtHight.error = "Hanya masukkan angka BODOH"
            }
            if (!isEmptyFields && !isInvalidDouble) {
                val volume = length as Double * width as Double * hight as Double
                tvResult.text = volume.toString()
            }
        }
    }
    private fun toDouble(str: String): Double? {
        return try {
            str.toDouble()
        } catch (e: NumberFormatException) {
            null
        }
    }
}
